package Part1;

import util.*;

/**
 * Execute tests of MarkovRunner with different Markov number values.
 * Where Markov number is the number of letters used to
 * determine the next character.
 */
public class MarkovRunner {

	/**
	 * Generate 500 letters long random text using MarkovZero algorithm.
	 *
	 * @param trainingFilePath full path to text file.
	 */
    public void runMarkovZero(String trainingFilePath) {
		SEFileUtil seFileUtil = new SEFileUtil(trainingFilePath);
		String st = seFileUtil.asString();
		st = st.replace('\n', ' ');
		MarkovZero markov = new MarkovZero();
		markov.setTraining(st);
		markov.setSeed(25);
		for(int k=0; k < 3; k++){
			String text = markov.getRandomText(500);
			printOut(text);
		}
	}

	/**
	 * Generate 500 letters long random text using MarkovOne algorithm.
	 *
	 * @param trainingFilePath full path to text file
	 */
	public void runMarkovOne(String trainingFilePath) {
		SEFileUtil seFileUtil = new SEFileUtil(trainingFilePath);
		String st = seFileUtil.asString();
		st = st.replace('\n', ' ');
		MarkovOne markov = new MarkovOne();
		markov.setTraining(st);
		markov.setSeed(25);
		for(int k=0; k < 3; k++){
			String text = markov.getRandomText(500);
			printOut(text);
		}
	}

	/**
	 * Generate 500 letters long random text using MarkovFour algorithm.
	 *
	 * @param trainingFilePath full path to text file
	 */
	public void runMarkovFour(String trainingFilePath) {
		SEFileUtil seFileUtil = new SEFileUtil(trainingFilePath);
		String st = seFileUtil.asString();
		st = st.replace('\n', ' ');
		MarkovFour markov = new MarkovFour();
		markov.setTraining(st);
		markov.setSeed(25);
		for (int k = 0; k < 3; k++) {
			String text = markov.getRandomText(500);
			printOut(text);
		}
	}

	/**
	 * Generate 500 letters long random text using MarkovModel N algorithm.
	 * Where N is the number of letters used for the algorithm.
	 * N is being initialized in the MarkovModel constructor.
	 *
	 * @param trainingFilePath full path to text file
	 */
	public void runMarkovModel(String trainingFilePath) {
		SEFileUtil seFileUtil = new SEFileUtil(trainingFilePath);
		String st = seFileUtil.asString();
		st = st.replace('\n', ' ');
		MarkovModel markov = new MarkovModel(6);
		markov.setTraining(st);
		markov.setSeed(25);
		for (int k = 0; k < 3; k++) {
			String text = markov.getRandomText(500);
			printOut(text);
		}
	}

	/**
	 * Print given text in pre-made format.
	 *
	 * @param s text to print
	 */
	private void printOut(String s){
		String[] words = s.split("\\s+");
		int psize = 0;
		System.out.println("----------------------------------");
		for(int k=0; k < words.length; k++){
			System.out.print(words[k]+ " ");
			psize += words[k].length() + 1;
			if (psize > 60) {
				System.out.println();
				psize = 0;
			}
		}
		System.out.println("\n----------------------------------");
	}

	public static void main(String[] args) {
		MarkovRunner markovRunner = new MarkovRunner();
		markovRunner.runMarkovZero(args[0]);
		markovRunner.runMarkovOne(args[0]);
		markovRunner.runMarkovFour(args[0]);
		markovRunner.runMarkovModel(args[0]);
	}
	
}
