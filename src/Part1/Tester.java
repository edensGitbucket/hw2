package Part1;

import util.*;

import java.util.ArrayList;

/**
 * Tester of getFollows function in MarkovOne
 */
public class Tester {
    public void testGetFollows() {
        MarkovOne tester = new MarkovOne();
        tester.setTraining("this is a test yes this is a test.");
        ArrayList<Character> follows = tester.getFollows(".");
        System.out.println(follows);
    }
}

