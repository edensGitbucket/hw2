package Part1;

import util.SEFileUtil;

import java.util.ArrayList;
import java.util.Random;

public class MarkovFour {
    private String myText;
    private Random myRandom;

    public MarkovFour() {
        myRandom = new Random();
    }

    /**
     * Sets the seed to the given seed
     *
     * @param seed the number used to initialize the random function
     */
    public void setSeed(int seed){
        myRandom = new Random(seed);
    }

    /**
     * Sets the training text to the given text.
     * @param s training text
     */
    public void setTraining(String s){
        myText = s.trim();
    }

    /**
     * Get the following characters of the given letter sequence.
     *
     * @param key required letter sequence
     * @return list of all the following characters of given sequence
     */
    public ArrayList<Character> getFollows(String key){
        ArrayList<Character> followers = new ArrayList<>();
        int searchOffset = 0;
        int keyIndex;
        while(true){
            keyIndex = myText.indexOf(key, searchOffset);
            if(keyIndex == -1 || keyIndex == myText.length()-4)
                break;
            followers.add(myText.charAt(keyIndex+4));
            searchOffset = keyIndex+1;
        }
        return followers;
    }

    /**
     * Generate a random sequence of characters from the text according to the
     * given number using the Markov algorithm.
     *
     * @param numChars number of characters to generate
     * @return a random text which it's length is numChars
     */
    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int index = myRandom.nextInt(myText.length()-4);
        for(int i=0; i<4; i++){
            sb.append(myText.charAt(index+i));
        }
        for(int k=4; k < numChars; k++) {
            ArrayList<Character> currentFollowers = getFollows(
                    sb.substring(sb.length() - 4, sb.length()));
            if (currentFollowers.size() > 0) {
                index = myRandom.nextInt(currentFollowers.size());
                sb.append(currentFollowers.get(index));
            }
        }
        return sb.toString();
    }
}
