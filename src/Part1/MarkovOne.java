package Part1;

import java.util.ArrayList;
import java.util.Random;

public class MarkovOne {
    private String myText;
    private Random myRandom;

    public MarkovOne() {
        myRandom = new Random();
    }

    /**
     * Sets the seed to the given seed
     *
     * @param seed the number used to initialize the random function
     */
    public void setSeed(int seed){
        myRandom = new Random(seed);
    }

    /**
     * Sets the training text to the given text.
     * @param s training text
     */
    public void setTraining(String s){
        myText = s.trim();
    }

    /**
     * Get the following characters of the given letter sequence.
     *
     * @param key required letter sequence
     * @return list of all the following characters of given sequence
     */
    public ArrayList<Character> getFollows(String key){
        ArrayList<Character> followers = new ArrayList<>();
        int searchOffset = 0;
        int keyIndex = 0;
        while(true){
            keyIndex = myText.indexOf(key, searchOffset);
            if(keyIndex == -1 || keyIndex == myText.length()-1)
                break;
            followers.add(myText.charAt(keyIndex+1));
            searchOffset = keyIndex+1;
        }
        return followers;
    }

    /**
     * Generate a random sequence of characters from the text according to the
     * given number using the Markov algorithm.
     *
     * @param numChars number of characters to generate
     * @return a random text which it's length is numChars
     */
    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        char currentLetter = myText.charAt(myRandom.nextInt(
                myText.length()-1));
        sb.append(currentLetter);
        for(int k=1; k < numChars; k++){
            ArrayList<Character> currentFollowers = getFollows(Character
                    .toString(currentLetter));
            currentLetter = currentFollowers.get(myRandom.nextInt(
                    currentFollowers.size()));
            sb.append(currentLetter);
        }

        return sb.toString();
    }
}