package Part1;

import java.util.ArrayList;
import java.util.Random;

public class MarkovZero {
    private String myText;
	private Random myRandom;
	
	public MarkovZero() {
		myRandom = new Random();
	}

	/**
	 * Sets the seed to the given seed
	 *
	 * @param seed the number used to initialize the random function
	 */
	public void setSeed(int seed){
		myRandom = new Random(seed);
	}

	/**
	 * Sets the training text to the given text.
	 * @param s training text
	 */
	public void setTraining(String s){
		myText = s.trim();
	}

	/**
	 * Generate a random sequence of characters from the text according to the
	 * given number using the Markov algorithm.
	 *
	 * @param numChars number of characters to generate
	 * @return a random text which it's length is numChars
	 */
	public String getRandomText(int numChars){
		if (myText == null){
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for(int k=0; k < numChars; k++){
			int index = myRandom.nextInt(myText.length());
			sb.append(myText.charAt(index));
		}
		
		return sb.toString();
	}
}