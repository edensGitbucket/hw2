package Part2;

import java.util.ArrayList;
import java.util.Random;

public class MarkovOne extends AbstractMarkovModel {
    private Random myRandom;

    public MarkovOne() {
        myRandom = new Random();
        markovNumber = 1;
    }

    public void setSeed(int seed){
        myRandom = new Random(seed);
    }

    public ArrayList<Character> getFollows(String key){
        ArrayList<Character> followers = new ArrayList<>();
        int searchOffset = 0;
        int keyIndex = 0;
        while(true){
            keyIndex = myText.indexOf(key, searchOffset);
            if(keyIndex == -1 || keyIndex == myText.length()-1)
                break;
            followers.add(myText.charAt(keyIndex+1));
            searchOffset = keyIndex+1;
        }
        return followers;
    }

    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        char currentLetter = myText.charAt(myRandom.nextInt(myText.length()-1));
        sb.append(currentLetter);
        for(int k=1; k < numChars; k++){
            ArrayList<Character> currentFollowers = getFollows(Character
                    .toString(currentLetter));
            currentLetter = currentFollowers.get(myRandom.nextInt(currentFollowers.size()));
            sb.append(currentLetter);
        }

        return sb.toString();
    }
}