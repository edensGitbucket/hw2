package Part2;

import java.util.ArrayList;
import java.util.Random;

public class MarkovModel extends AbstractMarkovModel {
    private Random myRandom;

    public MarkovModel(int markovNumber) {
        myRandom = new Random();
        this.markovNumber = markovNumber;
    }

    public void setSeed(int seed){
        myRandom = new Random(seed);
    }

    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int index = myRandom.nextInt(myText.length()-markovNumber);
        for(int i=0; i<markovNumber; i++){
            sb.append(myText.charAt(index+i));
        }
        for(int k=markovNumber; k < numChars; k++) {
            ArrayList<Character> currentFollowers = getFollows(sb.substring(sb.length() - markovNumber, sb.length()));
            if (currentFollowers.size() > 0) {
                index = myRandom.nextInt(currentFollowers.size());
                sb.append(currentFollowers.get(index));
            }
        }
        return sb.toString();
    }
}
