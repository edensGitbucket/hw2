package Part2;

import util.SEFileUtil;

/**
 * xecute tests of MarkovRunner with different Markov number values, using
 * IMarkovModel interface implemented modules and hash map.
 * Where Markov number is the number of letters used to determine
 * the next character.
 */
public class MarkovRunnerWithInterfaceEfficient {
    public void runModel(IMarkovModel markov, String text, int size, int seed) {
        markov.setTraining(text);
        markov.setSeed(seed);
        System.out.println("running with " + markov);
        for(int k = 0; k < 3; k++) {
            String st = markov.getRandomText(size);
            printOut(st);
        }
    }
    /**
     * Print given text in pre-made format.
     *
     * @param s text to print
     */
    private void printOut(String s) {
        String[] words = s.split("\\s+");
        int psize = 0;
        System.out.println("----------------------------------");
        for(int k = 0; k < words.length; k++) {
            System.out.print(words[k] + " ");
            psize += words[k].length() + 1;
            if (psize > 60) {
                System.out.println();
                psize = 0;
            }
        }
        System.out.println("\n----------------------------------");
    }

    public void testHashMap(String trainingFilePath, int seed) {
        SEFileUtil seFileUtil = new SEFileUtil(trainingFilePath);
        String st = seFileUtil.asString();
        st = st.replace('\n', ' ');
        int size = 200;
        EfficientMarkovModel emFive = new EfficientMarkovModel(5);
        runModel(emFive, st, size, seed);
    }

    public static void main(String[] args) {
        MarkovRunnerWithInterfaceEfficient markovRunnerInterface = new MarkovRunnerWithInterfaceEfficient();
        if (args.length != 2) {
            System.out.print("Please pass two arguments: 1.input_file 2.seed");
            System.exit(1);
        }
        try {
            int seed = Integer.parseInt(args[1]);
            markovRunnerInterface.testHashMap(args[0], seed);
        } catch(NumberFormatException e) {
            System.out.println("The second argument must be an integer");
            System.exit(1);
        }
    }
}
