package Part2;

import java.util.Random;

public interface IMarkovModel {
    /**
     * Sets the training text to the given text.
     * @param text training text
     */
    public void setTraining(String text);

    /**
     * Generate a random sequence of characters from the text according to the
     * given number.
     *
     * @param numChars number of characters to generate
     * @return a random text which it's length is numChars
     */
    public String getRandomText(int numChars);

    /**
     * Sets the seed to the given seed
     *
     * @param seed the number used to initialize the random function
     */
    public void setSeed(int seed);
    
}
