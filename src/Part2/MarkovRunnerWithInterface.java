package Part2;

import util.SEFileUtil;
/**
 * Execute tests of MarkovRunner with different Markov number values, using
 * IMarkovModel interface implemented modules.
 * Where Markov number is the number of letters used to determine
 * the next character.
 */
public class MarkovRunnerWithInterface {
    public void runModel(IMarkovModel markov, String text, int size, int seed) {
        markov.setTraining(text);
        markov.setSeed(seed);
        System.out.println("running with " + markov);
        for(int k=0; k < 3; k++){
			String st= markov.getRandomText(size);
			printOut(st);
		}
    }

	/**
	 * 	Generate 200 letters long random text using MarkovModel 0,1,3,4
	 * 	algorithm. Where the Markov number determines the amount of letters
	 * 	used for the algorithm.
	 *
	 * @param trainingFilePath full path to text file
	 * @param seed the number used to initialize the random function
	 */
    public void runMarkov(String trainingFilePath, int seed) {
		SEFileUtil seFileUtil = new SEFileUtil(trainingFilePath);
		String st = seFileUtil.asString();
		st = st.replace('\n', ' ');
		int size = 200;
		
        MarkovZero mz = new MarkovZero();
        runModel(mz, st, size, seed);
    
        MarkovOne mOne = new MarkovOne();
        runModel(mOne, st, size, seed);
        
        MarkovModel mThree = new MarkovModel(3);
        runModel(mThree, st, size, seed);
        
        MarkovFour mFour = new MarkovFour();
        runModel(mFour, st, size, seed);

    }
	/**
	 * Print given text in pre-made format.
	 *
	 * @param s text to print
	 */
	private void printOut(String s){
		String[] words = s.split("\\s+");
		int psize = 0;
		System.out.println("----------------------------------");
		for(int k=0; k < words.length; k++){
			System.out.print(words[k]+ " ");
			psize += words[k].length() + 1;
			if (psize > 60) {
				System.out.println();
				psize = 0;
			}
		}
		System.out.println("\n----------------------------------");
	}

	public static void main(String[] args) {
		MarkovRunnerWithInterface markovRunnerInterface = new MarkovRunnerWithInterface();
		if(args.length != 2)
		{
			System.out.print("Please pass two arguments: 1.input_file 2.seed");
			System.exit(1);
		}
		try {
			int seed = Integer.parseInt(args[1]);
			markovRunnerInterface.runMarkov(args[0], seed);
		} catch(NumberFormatException e) {
			System.out.println("The second argument must be an integer");
			System.exit(1);
		}
    }
}
