package Part2;

import java.util.*;

/**
 * Defines an abstract Markov model.
 */
public abstract class AbstractMarkovModel implements IMarkovModel {
    protected String myText;
    protected Random myRandom;
    protected int markovNumber;

    public AbstractMarkovModel() {
        myRandom = new Random();
    }
    
    final public void setTraining(String s) {
        myText = s.trim();
    }
 
    abstract public String getRandomText(int numChars);

    /**
     * Get the following characters of the given letter sequence.
     *
     * @param key required letter sequence
     * @return list of all the following characters of given sequence
     */
    protected ArrayList<Character> getFollows(String key){
        ArrayList<Character> followers = new ArrayList<>();
        int searchOffset = 0;
        int keyIndex;
        while(true){
            keyIndex = myText.indexOf(key, searchOffset);
            if(keyIndex == -1 || keyIndex == myText.length()-markovNumber)
                break;
            followers.add(myText.charAt(keyIndex+markovNumber));
            searchOffset = keyIndex+1;
        }
        return followers;
    }

    public String toString(){
        return ("MarkovModel of order " + markovNumber);
    }
}
