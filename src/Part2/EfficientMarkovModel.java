package Part2;

import java.util.ArrayList;
import java.util.Random;
import java.util.HashMap;

/**
 *  An alternative and more efficient way to execute the tests of MarkovRunner.
 *  This execute method uses a hash map to store the following character of each
 *  possible letter sequence.
 */
public class EfficientMarkovModel extends AbstractMarkovModel{
    private Random myRandom;
    HashMap<String, ArrayList<Character>> followersMap;

    public EfficientMarkovModel(int markovNumber) {
        myRandom = new Random();
        this.markovNumber = markovNumber;
        followersMap = new HashMap<>();
    }

    /**
     * Sets the seed to the given seed
     *
     * @param seed the number used to initialize the random
     */
    public void setSeed(int seed){
        myRandom = new Random(seed);
    }

    /**
     * Generate a random sequence of characters from the text according to the
     * given number.
     *
     * @param numChars number of characters to generate
     * @return a random text which it's length is numChars
     */
    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }
        buildMap();
        StringBuilder sb = new StringBuilder();
        int index = myRandom.nextInt(myText.length()-markovNumber);
        for(int i=0; i<markovNumber; i++){
            sb.append(myText.charAt(index+i));
        }
        for(int k = markovNumber; k < numChars; k++) {
            ArrayList<Character> currentFollowers = getFollows(
                    sb.substring(sb.length() - markovNumber, sb.length()));
            if (currentFollowers.size() > 0) {
                index = myRandom.nextInt(currentFollowers.size());
                sb.append(currentFollowers.get(index));
            }
        }
        return sb.toString();
    }

    /**
     * Build a hash map, with possible letter sequences as keys and following
     * characters of those sequences as values
     */
    public void buildMap() {
        if (myText.length() < markovNumber){
            return;
        }
        for(int k=0; k < myText.length() - markovNumber; k++) {
            String currWord = myText.substring(k, k + markovNumber);
            if (!followersMap.containsKey(currWord)) {
                ArrayList<Character> followers = new ArrayList<>();
                for (int i = k; i < myText.length() - markovNumber; i++) {
                    if (myText.substring(i, i + markovNumber).equals(currWord))
                        followers.add(myText.charAt(i + markovNumber));
                }
                followersMap.put(currWord, followers);
            }
        }
    }

    /**
     * Get the following characters of the given letter sequence from the hash
     * map.
     *
     * @param key required letter sequence
     * @return list of all the following characters of given sequence
     */
    public ArrayList<Character> getFollows(String key) {
        return followersMap.get(key);
    }

    public String toString(){
        return ("EfficientMarkovModel of order " + markovNumber);
    }
}

