package Part2;

import java.util.ArrayList;
import java.util.Random;

public class MarkovFour extends AbstractMarkovModel {
    private Random myRandom;

    public MarkovFour() {
        myRandom = new Random();
        markovNumber = 4;
    }

    public void setSeed(int seed){
        myRandom = new Random(seed);
    }

    public String getRandomText(int numChars){
        if (myText == null){
            return "";
        }
        StringBuilder sb = new StringBuilder();
        int index = myRandom.nextInt(myText.length()-4);
        for(int i=0; i<4; i++){
            sb.append(myText.charAt(index+i));
        }
        for(int k=4; k < numChars; k++) {
            ArrayList<Character> currentFollowers = getFollows(sb.substring(sb.length() - 4, sb.length()));
            if (currentFollowers.size() > 0) {
                index = myRandom.nextInt(currentFollowers.size());
                sb.append(currentFollowers.get(index));
            }
        }
        return sb.toString();
    }
}
